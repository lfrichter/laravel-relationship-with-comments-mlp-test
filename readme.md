# Laravel - Relationship with comments
@(lfrichter's notebook)[webdev, laravel]

[![php](https://img.shields.io/badge/php-7.2-777BB4.svg?logo=php&logoColor=white&style=for-the-badge)](http://php.net/) [ ![Laravel](https://img.shields.io/badge/laravel-5.8-E74430.svg?logo=laravel&logoColor=white&style=for-the-badge)](https://laravel.com/) [![mySql](https://img.shields.io/badge/mysql-5.7-4479A1.svg?logo=mysql&logoColor=white&style=for-the-badge) ](https://www.mysql.com/) [![nginx](https://img.shields.io/badge/nginx-1.14.0-269539.svg?logo=nginx&logoColor=white&style=for-the-badge)](http://nginx.org/) 
  
Now extending the above add a Comment Entity and on the User, create a relationship that gets all Post Comments belonging to a User.

## Models
### User
```php
<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = ['name', 'email', 'password',];
    protected $hidden = ['password', 'remember_token',];
    protected $casts = ['email_verified_at' => 'datetime',];

    /**
     * Relationship with post
     *
     * @return void
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     * Relationship with comment
     *
     * @return void
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
```

### Post
```php
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['user_id','title','text'];

    /**
     * Relationship with user
     *
     * @return void
     */
    public function user(){
        return $this->belongsTo(User::class);
    }

    /**
     * Relationship with comments
     *
     * @return void
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
```

### Comment
```php
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['user_id','post_id','text'];

    /**
     * Relationship with user
     *
     * @return void
     */
    public function user(){
        return $this->belongsTo(User::class);
    }

    /**
     * Relationship with post
     *
     * @return void
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

}
```

---
## Migration
### Comment
```php
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('post_id');
            $table->text('text');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}

```

## Factory
### Comment
```php
<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'text' => $faker->text(),
        'user_id' => factory('App\User')->create()->id,
        'post_id' => factory('App\Post')->create()->id,
    ];
});

```

## Seeder
### CreateCommentTable
```php
<?php

use App\Comment;
use Illuminate\Database\Seeder;

class CreateCommentTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Comment::class, 10)->create();
        factory(Comment::class, 1)->create(['user_id' => 5,'post_id' => 1]);
        factory(Comment::class, 1)->create(['user_id' => 6,'post_id' => 2]);
    }
}

```


---
## Tinker
### Testing
#### Relation from user to comments
```
namespace App
$first = User::first();
$first->comments
```
##### Result
```
=> Illuminate\Database\Eloquent\Collection {#2984
     all: [
       App\Comment {#2965
         id: 1,
         user_id: 1,
         post_id: 1,
         text: "Non eum quam qui voluptas in ullam perspiciatis ab. Asperiores modi expedita dolores fuga. Quia est quia est atque voluptas autem. Facere a esse molestiae fuga necessitatibus in voluptas.",
         created_at: "2019-06-20 15:54:52",
         updated_at: "2019-06-20 15:54:52",
       },
     ],
   }
>>>
```

#### Relation from posts and comments
```
namespace App
$u = User::find(2)
$posts = $u->posts
$posts[0]->comments
```
##### Result
```
=> Illuminate\Database\Eloquent\Collection {#2982
     all: [
       App\Comment {#2963
         id: 1,
         user_id: 1,
         post_id: 1,
         text: "Non eum quam qui voluptas in ullam perspiciatis ab. Asperiores modi expedita dolores fuga. Quia est quia est atque voluptas autem. Facere a esse molestiae fuga necessitatibus in voluptas.",
         created_at: "2019-06-20 15:54:52",
         updated_at: "2019-06-20 15:54:52",
       },
       App\Comment {#2985
         id: 11,
         user_id: 5,
         post_id: 1,
         text: "Nihil velit facilis rerum illo. Quos repellendus aut enim minima dolores tenetur.",
         created_at: "2019-06-20 15:54:52",
         updated_at: "2019-06-20 15:54:52",
       },
     ],
   }
```

#### Relation from comments to user and post
```
namespace App
$c = Comment::first()
$c->post
$c->user
```
##### Result to post
```
=> App\Post {#2971
     id: 1,
     user_id: 2,
     title: "Alias officiis explicabo laboriosam.",
     text: "Vitae voluptas voluptates ipsum quia animi et. Qui qui porro voluptas possimus. A fugiat tempore in. Blanditiis repudiandae fugiat eius est autem. Enim officia sint qui distinctio.",
     created_at: "2019-06-20 15:54:51",
     updated_at: "2019-06-20 15:54:51",
   }
```
##### Result to user
```
=> App\User {#2959
     id: 1,
     name: "Gail Hintz",
     email: "mark.gorczany@example.org",
     email_verified_at: "2019-06-20 15:54:51",
     created_at: "2019-06-20 15:54:51",
     updated_at: "2019-06-20 15:54:51",
   }
```



---
## Create a User and Post entity and relate them one to many
### migration

#### Post
```php
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();
            $table->string('title');
            $table->text('text');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
```

### Factory
#### Post
```php
<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(5),
        'text' => $faker->text(),
        'user_id' => factory('App\User')->create()->id,
    ];
});
```

### Seeder
#### CreatePostTable
```php
<?php

use App\Post;
use Illuminate\Database\Seeder;

class CreatePostTable extends Seeder
{
    public function run()
    {
        factory(Post::class, 10)->create();

        factory(Post::class, 1)->create([
            'user_id' => 1,
        ]);
    }
}
```
