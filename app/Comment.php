<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['user_id','post_id','text'];

    /**
     * Relationship with user
     *
     * @return void
     */
    public function user(){
        return $this->belongsTo(User::class);
    }

    /**
     * Relationship with post
     *
     * @return void
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

}
