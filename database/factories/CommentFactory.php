<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'text' => $faker->text(),
        'user_id' => factory('App\User')->create()->id,
        'post_id' => factory('App\Post')->create()->id,
    ];
});
