<?php

use App\Comment;
use Illuminate\Database\Seeder;

class CreateCommentTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Comment::class, 10)->create();
        factory(Comment::class, 1)->create(['user_id' => 5,'post_id' => 1]);
        factory(Comment::class, 1)->create(['user_id' => 6,'post_id' => 2]);
        // factory(Comment::class, 1)->create(['user_id' => 3]);
    }
}
