<?php

use App\Post;
use Illuminate\Database\Seeder;

class CreatePostTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Post::class, 10)->create();

        factory(Post::class, 1)->create([
            'user_id' => 1,
        ]);
    }
}
